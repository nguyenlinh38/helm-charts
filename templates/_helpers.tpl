{{/*meeyproject labels*/}}
{{- define "meeyproject.labels" -}}
app: meeyproject
{{- end}}

{{/*meeyproject selectorLabels*/}}
{{- define "meeyproject.selectorLables" -}}
app: meeyproject
{{- end}}

{{/*meeyproject labels namespace*/}}
{{- define "namespace.labels" -}}
cert-manager-tls: meeyproject
{{- end}}
